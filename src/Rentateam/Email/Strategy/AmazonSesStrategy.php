<?php

namespace Rentateam\Email\Strategy;

use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Rentateam\Email\Address\Address;
use Rentateam\Email\Address\AddressFactory;
use Rentateam\Email\Attachment;
use Rentateam\Email\Exception\Exception;
use Rentateam\Email\Message;
use Psr\Log\LoggerInterface;

class AmazonSesStrategy implements InterfaceStrategy
{
    /** @var \Aws\Ses\SesClient */
    protected $sesClient;
    /** @var  LoggerInterface|null */
    protected $logger;

    public function __construct(SesClient $sesClient, LoggerInterface $logger = null)
    {
        $this->sesClient = $sesClient;
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "Amazon SES";
    }

    /**
     * @param Message $message
     * @return bool
     * @throws Exception
     */
    public function send(Message $message)
    {
        $attachmentList = $message->getAttachmentList();
        if(!$attachmentList){
            try{
                // suppress php 7.1
                // Warning: A non-numeric value encountered in /var/www/html/vendor/aws/aws-sdk-php/src/Api/Serializer/QueryParamBuilder.php on line 108
                $errorReportingLevel = error_reporting();
                error_reporting($errorReportingLevel & ~E_NOTICE & ~E_WARNING);
                $response = $this->sesClient->sendEmail([
                    'Destination' => $this->getRecipientData($message),
                    'Message' => $this->getMessageData($message),
                    'ReplyToAddresses' => $this->getOptions($message),
                    'Source' => $this->getSender($message)
                ]);
                error_reporting($errorReportingLevel);
                return true;
            }catch(SesException $e){
                if($this->logger){
                    $this->logger->info('Letsee\Mailer\Strategy\AmazonSesStrategy::send failed',array(
                        'Destination' => $this->getRecipientData($message),
                        'Message' => $this->getMessageData($message),
                        'ReplyToAddresses' => $this->getOptions($message),
                        'Source' => $this->getSender($message),
                        'errorClass' => get_class($e),
                        'errorMessage' => $e->getMessage(),
                        'errorTrace' => $e->getTraceAsString()
                    ));
                }
                return false;
            }
        } else {
            $recipientAddresses = array();
            foreach ($message->getRecipientAddresses() as $recipient) {
                $recipientAddresses[] = (string) $this->createAddressObject($recipient)->getAddress(true);
            }

            try{
                // suppress php 7.1
                // Warning: A non-numeric value encountered in /var/www/html/vendor/aws/aws-sdk-php/src/Api/Serializer/QueryParamBuilder.php on line 108
                $errorReportingLevel = error_reporting();
                error_reporting($errorReportingLevel & ~E_NOTICE & ~E_WARNING);
                $response = $this->sesClient->sendRawEmail([
                    'Destinations' => $recipientAddresses,
                    'RawMessage' => $this->getRawEmail($message)
                ]);
                error_reporting($errorReportingLevel);
                return true;
            }catch(SesException $e){
                if($this->logger){
                    $this->logger->info('Rentateam\Email\Strategy\AmazonSesStrategy::send failed',array(
                        'Destinations' => $recipientAddresses,
                        'RawMessage' => $this->getRawEmail($message),
                        'errorClass' => get_class($e),
                        'errorMessage' => $e->getMessage(),
                        'errorTrace' => $e->getTraceAsString()
                    ));
                }
                return false;
            }
        }
    }

    /**
     * @param Message $message
     * @return string
     */
    protected function getSender(Message $message)
    {
        $sender = $message->getSenderAddress();

        return (string) $this->createAddressObject($sender)->getAddress(true);
    }

    /**
     * @param Message $message
     * @return array
     */
    protected function getOptions(Message $message)
    {
        $options = array();

        $replyTo = $message->getReplyToAddress();
        if ($replyTo) {
            $options['ReplyToAddresses'] = (string) $this->createAddressObject($replyTo)->getAddress(true);
        }

        return $options;
    }

    /**
     * @param $email string
     * @return Address
     */
    protected function createAddressObject($email)
    {
        if (!$email instanceof Address) {
            $factory = new AddressFactory();
            $email = $factory->create($email);
        }

        return $email;
    }

    /**
     * @param Message $message
     * @return array
     */
    protected function getMessageData(Message $message)
    {
        $messageData = array(
            'Subject' => array(
                'Data'    => (string) $message->getSubject(),
                'Charset' => (string) $message->getSubjectCharset()
            ),
            'Body' => array(
                'Html' => array(
                    'Data'    => (string) $message->getBodyHTML(),
                    'Charset' => (string) $message->getBodyHTMLCharset()
                )
            )
        );

        $messageData['Body']['Text'] = array(
            'Data'    => (string) $message->getBodyPlainText(),
            'Charset' => (string) $message->getBodyPlainTextCharset()
        );

        return $messageData;
    }

    /**
     * @param Message $message
     * @return array
     */
    protected function getRecipientData(Message $message)
    {
        $recipientData = array();
        $recipientData['ToAddresses'] = array();

        foreach ($message->getRecipientAddresses() as $recipient) {
            $recipientData['ToAddresses'][] = (string) $this->createAddressObject($recipient)->getAddress(true);
        }

        if ($recipientBlindCopyAddress = $message->getRecipientBlindCopyAddresses()) {
            $recipientData['BccAddresses'] = array();

            foreach ($recipientBlindCopyAddress as $recipientBlindCopy) {
                $recipientData['BccAddresses'][] = (string) $this->createAddressObject($recipientBlindCopy)->getAddress(true);
            }
        }

        return $recipientData;
    }

    /**
     * @param Message $message
     * @return array
     */
    protected function getRawEmail(Message $message)
    {
        $mail_mime = new \Mail_mime(array(
            "eol" => "\n",
            "html_charset" => (string) $message->getBodyHTMLCharset(),
            "text_charset" => (string) $message->getBodyPlainTextCharset(),
            "head_charset" => 'utf-8',
        ));
        $mail_mime->setTxtBody((string) $message->getBodyPlainText()."\n");
        $mail_mime->setHTMLBody((string) $message->getBodyHTML()."\n");
        foreach($message->getAttachmentList() as $attachment){
            /**
             * @var Attachment $attachment
             */
            $mail_mime->addAttachment(
                $attachment->getFilePath(),
                $attachment->getContentType(),
                $attachment->getFilename(),
                true,
                'base64',
                'attachment',
                '',
                '',
                '',
                'utf-8',
                '',
                '',
                'utf-8'
            );
        }

        //Retrieve the complete message body in MIME format, along with the headers:
        $body = $mail_mime->get();
        $headers = $mail_mime->txtHeaders(array(
            "From" => $this->getSender($message),
            "Subject" => (string) $message->getSubject()
        ));

        //Assemble the message using the headers and the body:
        $messageString = $headers . "\r\n" . $body;
        return array('Data' => $messageString);
    }
}