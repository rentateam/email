<?php
namespace Rentateam\Email\Strategy;

use Rentateam\Email\Attachment;
use Rentateam\Email\Exception\Exception;
use Rentateam\Email\Message;

class SwiftStrategy implements InterfaceStrategy
{
    /** @var \Swift_Mailer */
    protected $mailer;

    /**
     * @param \Swift_Mailer $mailer
     */
    public function setSwiftMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "Swift Mail";
    }

    /**
     * @param Message $message
     * @return bool
     * @throws Exception
     */
    public function send(Message $message)
    {
        $replyToAddress = $message->getReplyToAddress();
        $senderAddress = $message->getSenderAddress();
        $recipientAddressList = array();
        foreach($message->getRecipientAddresses() as $recipientAddress){
            $recipientAddressList[$recipientAddress->getEmail()] = $recipientAddress->getName();
        }

        $swiftMessage = \Swift_Message::newInstance();
        $swiftMessage->setSubject($message->getSubject())
                        ->setFrom(array($senderAddress->getEmail() => $senderAddress->getName()))
                        ->setTo($recipientAddressList)
                        ->setReplyTo(array($replyToAddress->getEmail() => $replyToAddress->getName()))
                        ->setBody($message->getBodyPlainText(), null, $message->getBodyPlainTextCharset());

        $swiftMessage->addPart($message->getBodyHTML(), 'text/html', $message->getBodyHTMLCharset());

        if($attachmentList = $message->getAttachmentList()){
            foreach($attachmentList as $attachment){
                if (!($attachment instanceof Attachment)) {
                    continue;
                }
                $swiftMessage->attach(
                    \Swift_Attachment::newInstance(
                        $attachment->getFileContents(),
                        $attachment->getFilename(),
                        $attachment->getContentType()
                    )
                );
            }
        }

        return $this->mailer->send($swiftMessage);
    }
}