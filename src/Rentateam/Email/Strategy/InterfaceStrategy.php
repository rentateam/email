<?php
namespace Rentateam\Email\Strategy;

use Rentateam\Email\Message;

interface InterfaceStrategy
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param Message $message
     * @return bool
     */
    public function send(Message $message);
}