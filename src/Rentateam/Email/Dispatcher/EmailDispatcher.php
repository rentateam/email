<?php
namespace Rentateam\Email\Dispatcher;

use Rentateam\Email\Adapter\AdapterInterface;
use Rentateam\Email\Business\BusinessEmailInterface;
use Rentateam\Email\Mailer;
use Rentateam\Email\Message;
use Rentateam\Utility\TextUtility;
use Psr\Log\LoggerInterface;

class EmailDispatcher implements InterfaceDispatcher
{
    /** @var Mailer */
    protected $mailer;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param Mailer $mailer
     * @param \Twig_Environment $twig
     * @param LoggerInterface|null $logger
     */
    public function __construct(Mailer $mailer,  \Twig_Environment $twig, LoggerInterface $logger = null)
    {
        $this->mailer = $mailer;
        $this->twig   = $twig;
        $this->logger = $logger;
    }

    /**
     * @param AdapterInterface $adapter
     * @return Message
     */
    protected function getMessage(AdapterInterface $adapter)
    {
        $message = new Message();
        $subject = new TextUtility($adapter->getSubject());
        $subject->replace("\n"," ");
        $message->setSenderAddress($adapter->getSenderAddress());
        $message->setRecipientAddresses($adapter->getRecipientAddresses());
        $message->setReplyToAddress($adapter->getReplyAddress());
        $message->setSubject($subject->__toString());
        $message->setSubjectCharset($adapter->getSubjectCharset());
        $message->setBodyHTML($adapter->getHTML());
        $message->setBodyHTMLCharset($adapter->getBodyHTMLCharset());
        $message->setBodyPlainText($adapter->getText());
        $message->setBodyPlainTextCharset($adapter->getBodyPlainTextCharset());

        if($attachmentList = $adapter->getAttachmentList()){
            $message->setAttachmentList($attachmentList);
        }

        return $message;
    }

    /**
     * @param BusinessEmailInterface $email
     * @return Message
     */
    public function send(BusinessEmailInterface $email)
    {
        $adapter = $email->getAdapter($this->twig);

        $message = $this->getMessage($adapter);

        if (count($message->getRecipientAddresses()) > 0) {
            $this->mailer->send($message);
        }
    }
}
