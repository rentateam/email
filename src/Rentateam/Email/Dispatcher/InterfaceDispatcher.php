<?php
namespace Rentateam\Email\Dispatcher;

use Rentateam\Email\Business\BusinessEmailInterface;
use Rentateam\Email\Message;

interface InterfaceDispatcher
{
    /**
     * @param \Rentateam\Email\Business\BusinessEmailInterface $email
     * @return Message
     */
    public function send(BusinessEmailInterface $email);
}