<?php
namespace Rentateam\Email\Address;

class Address
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $email;

    /**
     * @param string $email
     * @param string|null $name
     */
    public function __construct($email, $name = null)
    {
        $this->email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $this->name  = $this->filterName($name);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name string
     */
    public function setName($name)
    {
        $this->name = $this->filterName($name);
    }

    protected function filterName($name)
    {
        if(is_null($name)){
            return null;
        }

        $name = html_entity_decode(filter_var($name, FILTER_SANITIZE_STRING), ENT_QUOTES);
        return $name;
    }

    /**
     * @param $shouldEncodeName boolean
     * @return string
     */
    public function getAddress($shouldEncodeName = false)
    {
        if (is_null($this->name)) {
            $email = $this->email;
        } else {
            $name = $shouldEncodeName ? mb_encode_mimeheader($this->name, 'UTF-8', 'B') : $this->name;
            $email = "\"{$name}\" <{$this->email}>";
        }

        return $email;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getAddress();
    }
}