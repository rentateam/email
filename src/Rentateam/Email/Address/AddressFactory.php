<?php
namespace Rentateam\Email\Address;

class AddressFactory
{
    /**
     * @param string $addressRaw
     * @return Address
     */
    public function create($addressRaw)
    {
        $emailStartIndex = strpos($addressRaw, '<');
        if ($emailStartIndex) {
            $name = trim(substr($addressRaw, 0, $emailStartIndex - 1), "\" ");
            $email = trim(substr($addressRaw, $emailStartIndex - 1), " ><");
        } else {
            $name = null;
            $email = $addressRaw;
        }

        return new Address($email, $name);
    }
}