<?php
namespace Rentateam\Email;

use Rentateam\Email\Address\Address;

class Message
{
    protected $senderAddress;
    protected $recipientAddresses          = array();
    protected $recipientBlindCopyAddresses = array();
    protected $replyToAddress;

    protected $bodyHtml;
    protected $bodyPlainText;

    protected $subject;

    protected $subjectCharset;
    protected $bodyHTMLCharset;
    protected $bodyPlainTextCharset;

    protected $attachmentList;

    /**
    * @param \Rentateam\Email\Address\Address $sender
    * @return Message
    */
    public function setSenderAddress($sender)
    {
        $this->senderAddress = $sender;

        return $this;
    }

    /**
     * @return \Rentateam\Email\Address\Address
     */
    public function getSenderAddress()
    {
        return $this->senderAddress;
    }

    /**
    * @param array|\Rentateam\Email\Address\Address $recipient
    * @return Message
    */
    public function setRecipientAddresses($recipient)
    {
        if (false === is_array($recipient)) {
            $recipient = array($recipient);
        }

        $this->recipientAddresses = $recipient;

        return $this;
    }

    /**
     * @return \Rentateam\Email\Address\Address[]
     */
    public function getRecipientAddresses()
    {
        return $this->recipientAddresses;
    }

    /**
     * @param \Rentateam\Email\Address\Address $replyTo
     * @return $this
     */
    public function setReplyToAddress(Address $replyTo)
    {
        $this->replyToAddress = $replyTo;

        return $this;
    }

    /**
     * @return \Rentateam\Email\Address\Address
     */
    public function getReplyToAddress()
    {
        return $this->replyToAddress;
    }

    /**
    * @param array|\Rentateam\Email\Address\Address $recipient
    * @return Message
    */
    public function setRecipientBlindCopyAddresses($recipient)
    {
        if (false === is_array($recipient)) {
            $recipient = array($recipient);
        }

        $this->recipientBlindCopyAddresses = $recipient;

        return $this;
    }

    /**
     * @return \Rentateam\Email\Address\Address[]
     */
    public function getRecipientBlindCopyAddresses()
    {
        return $this->recipientBlindCopyAddresses;
    }

    /**
     * @param string $subject
     * @return Message
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $bodyHtml
     * @return Message
     */
    public function setBodyHTML($bodyHtml)
    {
        $this->bodyHtml = $bodyHtml;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyHTML()
    {
        return $this->bodyHtml;
    }

    /**
     * @param string $bodyPlainText
     * @return Message
     */
    public function setBodyPlainText($bodyPlainText)
    {
        $this->bodyPlainText = $bodyPlainText;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyPlainText()
    {
        return $this->bodyPlainText;
    }

    /**
     * @param string $charset
     * @return Message
     */
    public function setSubjectCharset($charset)
    {
        $this->subjectCharset = $charset;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubjectCharset()
    {
        return $this->subjectCharset;
    }

    /**
     * @param string $charset
     * @return Message
     */
    public function setBodyHTMLCharset($charset)
    {
        $this->bodyHTMLCharset = $charset;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyHTMLCharset()
    {
        return $this->bodyHTMLCharset;
    }

    /**
     * @param string $charset
     * @return Message
     */
    public function setBodyPlainTextCharset($charset)
    {
        $this->bodyPlainTextCharset = $charset;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyPlainTextCharset()
    {
        return $this->bodyPlainTextCharset;
    }

    /**
     * @param array $attachmentList
     * @return Message
     */
    public function setAttachmentList(array $attachmentList)
    {
        $this->attachmentList = $attachmentList;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttachmentList()
    {
        return $this->attachmentList;
    }
}