<?php
namespace Rentateam\Email;

use Rentateam\Email\Strategy\InterfaceStrategy;

class Mailer
{
    /** @var InterfaceStrategy[] */
    protected $strategies = array();

    /**
     * @param $strategy InterfaceStrategy
     */
    public function addStrategy(InterfaceStrategy $strategy)
    {
        $this->strategies[] = $strategy;
    }

    /**
     * @param Message $message
     * @return bool
     */
    public function send(Message $message)
    {
        $result = false;

        if ($this->isSendingAllowed($message)) {

            /** @var $strategy InterfaceStrategy */
            foreach ($this->strategies as $strategy) {
                $result = $strategy->send($message);
            }
        }

        return $result;
    }

    /**
     * @param Message $message
     * @return bool
     */
    protected function isSendingAllowed(Message $message)
    {
        return count(($message->getRecipientAddresses())) >= 1;
    }
}