<?php
namespace Rentateam\Email\Business;

use Rentateam\Email\Adapter\AdapterInterface;

interface BusinessEmailInterface
{
    /**
     * @param \Twig_Environment $twig
     * @return AdapterInterface
     */
    public function getAdapter(\Twig_Environment $twig);
}