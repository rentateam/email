<?php
namespace Rentateam\Email\Adapter;

use Rentateam\Email\Address\Address;
use Rentateam\Utility\TextUtility;

abstract class AbstractAdapter implements AdapterInterface
{
    /**
     * @var Address
     */
    protected $senderAddrress;
    /**
     * @var Address
     */
    protected $replyAddrress;

    /**
     * @param \Rentateam\Email\Address\Address $replyAddrress
     * @return AbstractAdapter
     */
    public function setReplyAddress($replyAddrress)
    {
        $this->replyAddrress = $replyAddrress;
        return $this;
    }

    /**
     * @return \Rentateam\Email\Address\Address
     */
    public function getReplyAddress()
    {
        return $this->replyAddrress;
    }

    /**
     * @param \Rentateam\Email\Address\Address $senderAddrress
     * @return AbstractAdapter
     */
    public function setSenderAddress($senderAddrress)
    {
        $this->senderAddrress = $senderAddrress;
        return $this;
    }

    /**
     * @return \Rentateam\Email\Address\Address
     */
    public function getSenderAddress()
    {
        return $this->senderAddrress;
    }

    /**
     * @return string
     */
    public function getText()
    {
        $text = new TextUtility($this->getHTML());

        return $text->toPlainText();
    }

    /**
     * @return string
     */
    public function getSubjectCharset()
    {
        return 'windows-1251';
    }

    /**
     * @return string
     */
    public function getBodyHTMLCharset()
    {
        return 'utf-8';
    }

    /**
     * @return string
     */
    public function getBodyPlainTextCharset()
    {
        return 'utf-8';
    }


    /**
     * @return array of Attachment
     */
    public function getAttachmentList()
    {
        return [];
    }
}