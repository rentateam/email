<?php
namespace Rentateam\Email\Adapter;

use Rentateam\Email\Business\BusinessEmailInterface;

abstract class AbstractTwigAdapter extends  AbstractAdapter
{
    /** @var BusinessEmailInterface */
    protected $email;

    /** @var \Twig_Environment */
    protected $twig;

    /**
     * @param \Twig_Environment $twig
     * @param BusinessEmailInterface $email
     */
    public function __construct(\Twig_Environment $twig, BusinessEmailInterface $email)
    {
        $this->twig = $twig;
        $this->email = $email;
    }

    /**
     * @return string
     */
    abstract public function getTwigTemplate();

    /**
     * @return array
     */
    public function getTwigVariables()
    {
        return array();
    }

    /**
     * @return string
     */
    public function getHTML()
    {
        return $this->twig->render($this->getTwigTemplate(), $this->getTwigVariables());
    }
}