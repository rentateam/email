<?php
namespace Rentateam\Email\Adapter;

use Rentateam\Email\Address\Address;

interface AdapterInterface
{
    /**
     * @return string
     */
    public function getSubject();

    /**
     * @return Address
     */
    public function getSenderAddress();

    /**
     * @return Address
     */
    public function getReplyAddress();

    /**
     * @return Address[]
     */
    public function getRecipientAddresses();

    /**
     * @return string
     */
    public function getHTML();

    /**
     * @return string
     */
    public function getText();

    /**
     * @return string
     */
    public function getSubjectCharset();

    /**
     * @return string
     */
    public function getBodyHTMLCharset();

    /**
     * @return string
     */
    public function getBodyPlainTextCharset();

    /**
     * @return array of Attachment
     */
    public function getAttachmentList();
}