<?php

namespace Rentateam\Email;


class Attachment
{
    /**
     * @var string
     */
    protected $contentType;
    /**
     * @var string
     */
    protected $filePath;
    /**
     * @var string
     */
    protected $filename;

    /**
     * @param string $contentType
     * @return Attachment
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param string $filePath
     * @return Attachment
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param string $filename
     * @return Attachment
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Содержимое файла
     * @return string
     */
    public function getFileContents()
    {
        return file_get_contents($this->filePath);
    }
}