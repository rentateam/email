<?php
namespace Rentateam\Utility;

class TextUtility
{
    /** @var string */
    protected $value;

    /**
     * @param string $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }

    /**
     * @param integer $maxSymbols
     * @return $this
     */
    public function shorten($maxSymbols)
    {
        $strLen = mb_strlen($this->value,'UTF-8');
        if ($strLen < $maxSymbols) {
            return $this;
        }

        $this->value = mb_substr($this->value, 0, $maxSymbols,'UTF-8') . '…';

        return $this;
    }

    /**
     * @param null $allowed
     * @return $this
     */
    public function stripTags($allowed = null)
    {
        $this->value = strip_tags($this->value, $allowed);

        return $this;
    }

    /**
     * @return $this
     */
    public function removeDoublingSpaces()
    {
        $this->value = preg_replace('/\s{2,}/', ' ', $this->value);

        return $this;
    }

    /**
     * @param string $separator
     * @return $this
     */
    public function toSingleLine($separator = ' ')
    {
        $this->value = str_replace(array("\r", "\n"), $separator, $this->value);

        return $this;
    }

    /**
     * @return $this
     */
    public function trim()
    {
        $this->value = preg_replace( "/(^\s+)|(\s+$)/us", "", $this->value );

        return $this;
    }

    /**
     * @return $this
     */
    public function removeSpacesAfterNL()
    {
        $this->value = str_replace( "\n ", "\n", $this->value );
        $this->value = str_replace( "\r\n ", "\r\n", $this->value );

        return $this;
    }

    /**
     * @return $this
     */
    public function toPlainCode()
    {
        $this->value = trim(preg_replace("'\s+'", ' ', $this->value));

        return $this;
    }

    /**
     * @return $this
     */
    public function replaceBRtoNL()
    {
        $this->value = preg_replace('#<br\s*/?>#i', "\n", $this->value);

        return $this;
    }

    /**
     * @return $this
     */
    public function replaceNLtoBR()
    {
        $this->value = str_replace( "\r\n", "<br />", $this->value );
        $this->value = str_replace( "\n", "<br />", $this->value );

        return $this;
    }

    /**
     * @return $this
     */
    public function escape()
    {
        $this->value = htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8');

        return $this;
    }

    /**
     * @return $this
     */
    public function capitalizeFirst()
    {
        $this->value = mb_strtoupper(mb_substr($this->value, 0, 1), 'UTF-8') . mb_substr($this->value, 1);

        return $this;
    }

    /**
     * @return $this
     */
    public function removeTabs()
    {
        $this->value = str_replace("\t", "", $this->value);

        return $this;
    }

    /**
     * @param string $from
     * @param string $to
     * @return $this
     */
    public function replace($from, $to)
    {
        $this->value = str_replace($from, $to, $this->value);

        return $this;
    }

    /**
     * @return $this
     */
    public function replaceNLtoP()
    {
        $array = explode("\n", $this->value);
        foreach ($array as &$item) {
            if (in_array(substr($item, 0, 3), array("<ul", "<ol", "<if"))) {
                continue;
            } else {
                $item = "<p>{$item}</p>";
            }
        }

        $this->value = implode("", $array);

        return $this;
    }

    /**
     * @return $this
     */
    public function toPlainText()
    {
        preg_match_all("/<[Aa][\s]{1}[^>]*[Hh][Rr][Ee][Ff][^=]*=[ '\"\s]*([^ \"'>\s#]+)[^>]*>/", $this->value, $urls);

        $text = $this->value;
        foreach($urls[0] as $key => $value) {
            $text = str_replace($value, $urls[1][$key] . " ", $text);
        }

        $this->value = trim(strip_tags($text));

        return $this;
    }

    public function replaceNbsp()
    {
        $this->value = str_replace("&nbsp;", " ", $this->value);

        return $this;
    }
}
