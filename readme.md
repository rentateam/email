#Рассылка писем

Цель библиотеки - дать возможность создания email с кастомным текстовым и html наполнением и позволить его отправлять разными транспортами. Поддерживается swiftmailer.

BusinessEmailInterface - предназначен для инкапсуляции бизнес-логики писем. Конструирует адаптер, который ответственен за содержимое письма.

AdapterInterface - отвечает за кастомизацию внутренностей представления письма. Есть стандартная реализация для twig-шаблона (AbstractTwigAdapter).

EmailDispatcher - диспетчер отправки писем. Отправляет сконстриуированный объект 
BusinessEmailInterface.

Mailer - контейнер для стратегий отправки. Последовательно пытается отправить письмо с помощью стратегий-InterfaceStrategy.

Пример отправки письма 


```sh
  $email = new MyEmail();
  $email->setRecepientAddress($emailList)
                ->setSenderAddress($senderAddress)
                ->setBusinessLogic($someBusinessLogic);
  /** @var EmailDispatcher emailDispatcher **/
  $emailDispatcher->send($email);
```